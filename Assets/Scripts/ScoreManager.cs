﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] GameObject[] lives;
    [SerializeField] GameObject[] extralives;
    [SerializeField] TextMeshProUGUI score;

    private int scoreInt=0;
    private int currentLives = 3;
    private int currentExtraLives = 0;
    private int contador = 0;

    // Start is called before the first frame update
    void Start()
    {
        scoreInt = 0;
        currentLives = 3;
        score.text = "000000";// scoreInt.ToString("000000");
        //score.text = scoreInt.ToString();
    }

    public void AddScore(int value){
        scoreInt+=value;
        score.text = scoreInt.ToString("000000");
        
    }

   

    public void LoseLife()
    {
        currentLives--;
        if (currentLives >=0)
        {
            if (contador<=3) {
                lives[currentLives].SetActive(false);
                contador++;
            }
        }
        else if (currentLives < 0)
        {

                extralives[currentExtraLives].SetActive(false);

        }

    }
    public void LoseExtraLife()
    {
        
        
    }


    public void AddLife()
    {
        //currentLives++;
        currentExtraLives++;
        if (currentExtraLives >= 0)
        {
            extralives[currentExtraLives].SetActive(true);
        }
    }

}
