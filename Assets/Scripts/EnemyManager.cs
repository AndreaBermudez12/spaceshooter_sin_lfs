﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject[] formaciones;
    public GameObject[] PoweUp;
    public GameObject[] Boss;
    public float timeLaunchFormation;
   // public float timeLaunchPowerUp;
    public float timeLaunchBoss;
    public int[] pesos;

    private float currentTime = 0;

    void Awake(){
        StartCoroutine(LanzaFormacion());
    }

    public int GetRandomWeightedIndex(int[] weights)
    {
        // Get the total sum of all the weights.
        int weightSum = 0;
        for (int i = 0; i < weights.Length; ++i)
        {
            weightSum += weights[i];
        }

        // Step through all the possibilities, one by one, checking to see if each one is selected.
        int index = 0;
        int lastIndex = weights.Length - 1;
        while (index < lastIndex)
        {
            // Do a probability check with a likelihood of weights[index] / weightSum.
            if (Random.Range(0, weightSum) < weights[index])
            {
                return index;
            }

            // Remove the last item from the sum of total untested weights and try again.
            weightSum -= weights[index++];
        }

        // No other item was selected, so return very last index.
        return index;
    }

    IEnumerator LanzaFormacion(){
        int formacionActual=0;
        int formacionUp= 0;
        int oleada = 0;
        while(oleada<20){
            //formacionActual = Random.Range(0,formaciones.Length); 
            formacionActual = GetRandomWeightedIndex(pesos);

            //Debug.Log(formacionActual);


            Instantiate(formaciones[formacionActual],new Vector3(8,Random.Range(-4,4)),Quaternion.identity,this.transform);
          
            if (oleada == 5)
            {
                Instantiate(PoweUp[formacionUp], new Vector3(8, Random.Range(-4, 4)), Quaternion.identity, this.transform);
            }
            if (oleada == 10)
            {
                Instantiate(PoweUp[formacionUp], new Vector3(8, Random.Range(-4, 4)), Quaternion.identity, this.transform);
            }
            if (oleada == 15)
            {
                Instantiate(PoweUp[formacionUp], new Vector3(8, Random.Range(-4, 4)), Quaternion.identity, this.transform);
            }

            yield return new WaitForSeconds(timeLaunchFormation);
            oleada++;
        }   
        int bossactual = 0;
        Instantiate(Boss[bossactual], new Vector3(8, Random.Range(0, 0)), Quaternion.identity, this.transform);
    }
        
}