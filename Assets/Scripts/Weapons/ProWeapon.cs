﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProWeapon : Weapon
{

    public GameObject ProBullet;
    public float cadenciaPro;

    public AudioSource audioSource;

    public override float GetCadencia()
    {
        return cadenciaPro;
    }

    public override void Shoot()
    {
        Instantiate (ProBullet, this.transform.position, Quaternion.identity, null);
        
        //Play audio
        audioSource.Play();
    }
}
