﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    [SerializeField] GameObject[] sprites;
    private int elegido;

    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;

    private float timeCounter;
    public float timeToShoot;

    private float speedx;

    private bool isShooting;

    private ScoreManager sm;

    public int puntuacion = 100;
    public int vida = 100;
    public int angulo;
    private bool direccion = true;


    [SerializeField] GameObject bullet;

    private void Awake()
    {

        elegido = Random.Range(0, sprites.Length);

        for (int kk = 0; kk < sprites.Length; kk++)
        {
            sprites[kk].SetActive(false);
        }

        sprites[elegido].SetActive(true);
        
        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();

        Inicitialization();
    }

    protected virtual void Inicitialization(){
        timeCounter = 0.0f;
        speedx = 0.0f;
        isShooting = false;
    }

    protected virtual void EnemyBehaviour(){
        timeCounter += Time.deltaTime;

        if (timeCounter > timeToShoot)
        {
            
                Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, angulo+90), null);
                if (angulo > 180)
                {
                    direccion = false;
                }
                if (angulo < 0)
                {
                    direccion = true;
                }
                if (direccion == true)
                {
                    angulo += 10;
                }
                else
                {
                    angulo -= 10;
                }
            timeCounter = 0;
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        EnemyBehaviour();
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Bullet") {
            //StartCoroutine(DestroyShip());
            vida = vida - 10;
            if (vida == 0)
            {
                StartCoroutine(DestroyShip());
            }
        }else if(other.tag == "Finish"){
           // Destroy(this.gameObject);
        }
    }

    IEnumerator DestroyShip(){
        //Sumar puntos
        sm.AddScore(puntuacion);

        //Desactivo el grafico
        sprites[elegido].SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("GameOver");

        Destroy(this.gameObject);
    }
}
