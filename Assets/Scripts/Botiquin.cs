﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botiquin : MonoBehaviour
{
    public GameObject[] formaciones;
    public float timeLaunchFormation;
    public int[] pesos;

    private float currentTime = 0;

    void Awake()
    {
        StartCoroutine(LanzaFormacion());
    }

    public int GetRandomWeightedIndex(int[] weights)
    {
        // Get the total sum of all the weights.
        int weightSum = 0;
        for (int i = 0; i < weights.Length; ++i)
        {
            weightSum += weights[i];
        }

        // Step through all the possibilities, one by one, checking to see if each one is selected.
        int index = 0;
        int lastIndex = weights.Length - 1;
        while (index < lastIndex)
        {
            // Do a probability check with a likelihood of weights[index] / weightSum.
            if (Random.Range(0, weightSum) < weights[index])
            {
                return index;
            }

            // Remove the last item from the sum of total untested weights and try again.
            weightSum -= weights[index++];
        }

        // No other item was selected, so return very last index.
        return index;
    }

    IEnumerator LanzaFormacion()
    {
        int formacionActual = 0;

        //formacionActual = Random.Range(0,formaciones.Length); 
        formacionActual = GetRandomWeightedIndex(pesos);

        //Debug.Log(formacionActual);


        Instantiate(formaciones[formacionActual], new Vector3(8, Random.Range(-5, 5)), Quaternion.identity, this.transform);
        yield return new WaitForSeconds(timeLaunchFormation);
        
    }
}
